namespace AngularDashboardTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addAjaxUrl : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Widgets", "DataUrl", c => c.String());
            AddColumn("dbo.Widgets", "UpdateInterval", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Widgets", "UpdateInterval");
            DropColumn("dbo.Widgets", "DataUrl");
        }
    }
}
