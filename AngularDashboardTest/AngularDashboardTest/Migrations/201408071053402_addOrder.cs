namespace AngularDashboardTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addOrder : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.WidgetPerUsers", "Order", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.WidgetPerUsers", "Order");
        }
    }
}
