namespace AngularDashboardTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addWidgetsPerUser : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.WidgetPerUsers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        WidgetId = c.Int(nullable: false),
                        UserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .ForeignKey("dbo.Widgets", t => t.WidgetId, cascadeDelete: true)
                .Index(t => t.WidgetId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.WidgetPerUsers", "WidgetId", "dbo.Widgets");
            DropForeignKey("dbo.WidgetPerUsers", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.WidgetPerUsers", new[] { "UserId" });
            DropIndex("dbo.WidgetPerUsers", new[] { "WidgetId" });
            DropTable("dbo.WidgetPerUsers");
        }
    }
}
