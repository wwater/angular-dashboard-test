using AngularDashboardTest.Models;

namespace AngularDashboardTest.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<AngularDashboardTest.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(AngularDashboardTest.Models.ApplicationDbContext context)
        {
            var hroffice = new Application {Id=0,Name = "HROffice"};
            var digiflex = new Application {Id=1,Name = "Digiflex" };

            context.Applications.AddOrUpdate(
            a => a.Name,
                hroffice,
                digiflex
            );

            context.Widgets.AddOrUpdate(
                w=>w.Name,
                new Widget()
                {
                    Application = hroffice,
                    Name = "People on leave",
                    SubTitle = "People on leave for now",
                    Type = WidgetType.Number,
                    DataUrl = "/api/RandomInts/GetRandomInt/?id=4",
                    UpdateInterval = 15000,
                    Size = 1
                },
                new Widget()
                {
                    Application = hroffice,
                    Name = "People on school",
                    SubTitle = "People on school for now",
                    Type = WidgetType.Number,
                    DataUrl = "/api/RandomInts/GetRandomInt/?id=30",
                    UpdateInterval = 4000,
                    Size = 1
                },
                new Widget()
                {
                    Application = digiflex,
                    Name = "Hours left",
                    SubTitle = "Aantal per dag",
                    Type = WidgetType.Bar,
                    DataUrl = "/api/RandomInts/GetRandomGraph/?id=7",
                    UpdateInterval = 4000,
                    Size = 1
                },
                new Widget()
                {
                    Application = digiflex,
                    Name = "Dikke graph",
                    SubTitle = "Aantal per week",
                    Type = WidgetType.Bar,
                    DataUrl = "/api/RandomInts/GetRandomGraph/?id=3",
                    UpdateInterval = 10000,
                    Size = 1
                },
                new Widget()
                {
                    Application = digiflex,
                    Name = "Week amount",
                    SubTitle = "Echt man",
                    Type = WidgetType.Number,
                    DataUrl = "/api/RandomInts/GetRandomInt/?id=200",
                    UpdateInterval = 5000,
                    Size = 2
                }
                );


        }
    }
}
