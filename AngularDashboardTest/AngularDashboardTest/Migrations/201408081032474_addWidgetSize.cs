namespace AngularDashboardTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addWidgetSize : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Widgets", "Size", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Widgets", "Size");
        }
    }
}
