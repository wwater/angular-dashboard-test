// <auto-generated />
namespace AngularDashboardTest.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.0-30225")]
    public sealed partial class addWidgetsPerUser : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(addWidgetsPerUser));
        
        string IMigrationMetadata.Id
        {
            get { return "201408071017270_addWidgetsPerUser"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
