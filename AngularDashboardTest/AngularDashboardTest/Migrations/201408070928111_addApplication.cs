namespace AngularDashboardTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addApplication : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Applications",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Widgets", "ApplicationId", c => c.Int(nullable: false));
            CreateIndex("dbo.Widgets", "ApplicationId");
            AddForeignKey("dbo.Widgets", "ApplicationId", "dbo.Applications", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Widgets", "ApplicationId", "dbo.Applications");
            DropIndex("dbo.Widgets", new[] { "ApplicationId" });
            DropColumn("dbo.Widgets", "ApplicationId");
            DropTable("dbo.Applications");
        }
    }
}
