﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AngularDashboardTest.Models
{
    public class WidgetPerUser : EntityBase
    {
        public int WidgetId { get; set; }
        public virtual Widget Widget { get; set; }

        public string UserId { get; set; }
        public virtual ApplicationUser User { get; set; }

        public int Order { get; set; }
    }
}