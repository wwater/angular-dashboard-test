﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AngularDashboardTest.Models
{
    public enum WidgetType
    {
        Bar,
        Number,
        Pie,
        Line
    }
}