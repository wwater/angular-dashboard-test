﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AngularDashboardTest.Models
{
    public class Application : EntityBase
    {
        public string Name { get; set; }

        public virtual List<Widget> Widgets { get; set; }
    }
}