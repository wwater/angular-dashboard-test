﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AngularDashboardTest.Models
{
    public class Widget : EntityBase
    {
        public string Name { get; set; }
        public string SubTitle { get; set; }
        public WidgetType Type { get; set; }
        public string DeepLink { get; set; }

        public int ApplicationId { get; set; }
        public virtual Application Application { get; set; }

        public virtual List<WidgetPerUser> UsersForWidget { get; set; }

        public string DataUrl { get; set; }
        public int UpdateInterval { get; set; }

        public int Size { get; set; }
    }
}