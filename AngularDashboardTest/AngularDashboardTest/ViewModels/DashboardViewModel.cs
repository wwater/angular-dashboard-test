﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AngularDashboardTest.Models;

namespace AngularDashboardTest.ViewModels
{
    public class DashboardViewModel
    {
        public List<WidgetDashbaordViewModel> ActiveWidgets { get; set; }
        public MenuViewModel MenuViewModel { get; set; }

        public DashboardViewModel()
        {
            MenuViewModel = new MenuViewModel();
            ActiveWidgets = new List<WidgetDashbaordViewModel>();
        }

        public void AddWidgetsPerUser(List<WidgetPerUser> widgetPerUser)
        {
            ActiveWidgets.AddRange(widgetPerUser.OrderBy(wpu =>wpu.Order).Select(w => WidgetDashbaordViewModel.CreateFrom(w.Widget)));
        }
    }

    public class MenuViewModel
    {
        public MenuViewModel()
        {
            Applications = new List<ApplicationViewModel>();
        }

        public List<ApplicationViewModel> Applications { get; set; }

        public void AddApplications(List<Application> apps)
        {
            Applications.AddRange(apps.Select(ApplicationViewModel.CreateFrom));
        }
    }

    public class ApplicationViewModel
    {
        public string Name { get; set; }
        public List<WidgetViewModel> Widgets { get; set; }

        public static ApplicationViewModel CreateFrom(Application a)
        {
            return new ApplicationViewModel()
            {
                Name = a.Name,
                Widgets = a.Widgets.Select(WidgetViewModel.CreateFrom).ToList()
            };

        }
    }

    public class WidgetViewModel
    {
        public string Name { get; set; }
        public string SubTitle { get; set; }
        public int Id { get; set; }

        public static WidgetViewModel CreateFrom(Widget w)
        {
            return new WidgetViewModel()
            {
                Id = w.Id,
                Name = w.Name,
                SubTitle = w.SubTitle
            };
 
        }
    }

    public class WidgetDashbaordViewModel
    {
        public string Name { get; set; }
        public string SubTitle { get; set; }
        public int Id { get; set; }
        public string Type { get; set; }
        public string DataUrl { get; set; }
        public int UpdateInterval { get; set; }
        public int Size { get; set; }

        public static WidgetDashbaordViewModel CreateFrom(Widget w)
        {
            return new WidgetDashbaordViewModel()
            {
                Id = w.Id,
                Name = w.Name,
                Type = w.Type.ToString(),
                SubTitle = w.SubTitle,
                DataUrl = w.DataUrl,
                UpdateInterval = w.UpdateInterval,
                Size = w.Size
            };

        }
    }
}