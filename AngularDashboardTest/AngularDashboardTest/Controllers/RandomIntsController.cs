﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AngularDashboardTest.Controllers
{
    public class RandomIntsController : ApiController
    {
        private static readonly Random Random = new Random();

        public IHttpActionResult GetRandomInt(int id)
        {
            return Ok(Random.Next(0, id+1));        
        }

        public IHttpActionResult GetRandomGraph(int id)
        {
            List<int> ints = new List<int>();
            for (int i = 0; i < id; i++)
            {
                ints.Add(Random.Next(0, id + 1));
            }
            return Ok(ints);
        }
    }
}
