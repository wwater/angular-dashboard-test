﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AngularDashboardTest.Models;

namespace AngularDashboardTest.Controllers
{
    public class WidgetsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Widgets
        public IQueryable<Widget> GetWidgets()
        {
            return db.Widgets;
        }

        // GET: api/Widgets/5
        [ResponseType(typeof(Widget))]
        public IHttpActionResult GetWidget(int id)
        {
            Widget widget = db.Widgets.Find(id);
            if (widget == null)
            {
                return NotFound();
            }

            return Ok(widget);
        }

        // PUT: api/Widgets/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutWidget(int id, Widget widget)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != widget.Id)
            {
                return BadRequest();
            }

            db.Entry(widget).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!WidgetExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Widgets
        [ResponseType(typeof(Widget))]
        public IHttpActionResult PostWidget(Widget widget)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Widgets.Add(widget);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = widget.Id }, widget);
        }

        // DELETE: api/Widgets/5
        [ResponseType(typeof(Widget))]
        public IHttpActionResult DeleteWidget(int id)
        {
            Widget widget = db.Widgets.Find(id);
            if (widget == null)
            {
                return NotFound();
            }

            db.Widgets.Remove(widget);
            db.SaveChanges();

            return Ok(widget);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool WidgetExists(int id)
        {
            return db.Widgets.Count(e => e.Id == id) > 0;
        }
    }
}