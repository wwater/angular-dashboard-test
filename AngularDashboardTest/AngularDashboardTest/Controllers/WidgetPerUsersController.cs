﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AngularDashboardTest.Models;
using AngularDashboardTest.ViewModels;
using Microsoft.AspNet.Identity;

namespace AngularDashboardTest.Controllers
{
    public class WidgetPerUsersController : ApiController
    {
        [HttpPost]
        public IHttpActionResult AddWidgetPerUser(int id)
        {
            WidgetPerUser widgetPerUser = null;
            using (var db = new ApplicationDbContext())
            {
                var userId = User.Identity.GetUserId();
                if (db.WidgetsPerUser.FirstOrDefault(w => w.WidgetId == id && w.UserId == userId) != null)
                {
                    return Conflict();
                }
                widgetPerUser = new WidgetPerUser()
                {
                    UserId = userId,
                    WidgetId = id,
                    Widget = db.Widgets.FirstOrDefault(w=>w.Id == id)
                };
                db.WidgetsPerUser.Add(widgetPerUser);
                db.SaveChanges();
            }
            return Ok(WidgetDashbaordViewModel.CreateFrom(widgetPerUser.Widget));
        }

        [HttpPost]
        public IHttpActionResult ChangeOrder(string stringIds)
        {
            List<int> ids = stringIds.Split(',').Select(int.Parse).ToList();
            string userId = User.Identity.GetUserId();
            using (var db = new ApplicationDbContext())
            {
                foreach (var wpu in db.WidgetsPerUser.Where(wpu => wpu.User.Id == userId))
                {
                    wpu.Order = ids.IndexOf(wpu.WidgetId);
                }
                db.SaveChanges();
            }
            return Ok();
        }
    }
}