﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AngularDashboardTest.Models;
using AngularDashboardTest.ViewModels;
using Microsoft.AspNet.Identity;

namespace AngularDashboardTest.Controllers
{
    public class HomeController : Controller
    {
        [Authorize]
        public ActionResult Index()
        {                
            var vm = new DashboardViewModel();
            using (var context = new ApplicationDbContext())
            {
                vm.MenuViewModel.AddApplications(context.Applications.ToList());
                vm.AddWidgetsPerUser(context.WidgetsPerUser.ToList().Where(wpu => wpu.User.UserName == User.Identity.GetUserName()).ToList());
            }
            return View(vm);
        }
    }
}