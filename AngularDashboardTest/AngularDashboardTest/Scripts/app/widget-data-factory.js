﻿var WidgetDataFactory = {};
WidgetDataFactory.Create = function (widgetType) {
    switch (widgetType) {
        case 'Bar':
            return ChartFactory.CreateBarChart();
        case 'Line':
            return ChartFactory.CreateLineChart();
        case 'Number':
            var widgetDataModel = {};
            widgetDataModel.value = null;
            widgetDataModel.update = function (data) {
                widgetDataModel.value = data;
            };
            return widgetDataModel;
        default:
            return null;
    }
}