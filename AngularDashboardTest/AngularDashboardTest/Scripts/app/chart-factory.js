﻿var ChartFactory = {};

ChartFactory.CreateBarChart = function() {
    var widgetDataModel = {};
    widgetDataModel.chart = {
        type: "ColumnChart",
        data: {
            cols: [
                { id: "month", label: "Month", type: "string" },
                { id: "laptop-id", label: "Laptop", type: "number" },
            ],
            rows: []
        },
        options: {
            isStacked: "true",
            fill: 20,
            displayExactValues: true,
            vAxis: {
                gridlines: {
                    count: 6
                },
                textStyle: {
                    color: "white"
                },
                baselineColor: 'white'
            },
            colors: ["white"],
            animation: {
                duration: 1000,
                easing: 'out',
            },
            hAxis: {
                gridlines: {
                    count: 6
                },
                textPosition: 'none'
            },
            height: 300,
            backgroundColor: "#9b9bce",
            legend: {
                position: "none"
            }
        },
        formatters: {},
    };
    widgetDataModel.update = function (data) {
        for (var i = 0; i < data.length; i++) {
            if (widgetDataModel.chart.data.rows.length == data.length) {
                widgetDataModel.chart.data.rows[i].c[1].v = data[i];
            } else {
                widgetDataModel.chart.data.rows.push({
                    c: [
                        { v: i + "" },
                        { v: data[i] }
                    ]
                });
            }
        }          
    };
    return widgetDataModel;
}

ChartFactory.CreateLineChart = function() {
    var widgetDataModel = ChartFactory.CreateBarChart();
    widgetDataModel.chart.type = "LineChart";
    return widgetDataModel;
}