var widgetsPreload = [];

dashboardApp.controller('DashboardController', function ($scope) {

        $scope.widgets = [];
        
        $scope.addWidgetToApi = function (widgetid)
        {
            $.post('/api/WidgetPerUsers/AddWidgetPerUser/?id=' + widgetid, { id: widgetid }, function (response) {
                $scope.addWidget(response);
            }, 'json');
        };
        $scope.saveOrderToApi = function (widgetid) {
            var ids = [];
            angular.forEach($scope.widgets, function (value, key) {
                ids.push(value.id);
            });
            $.post('/api/WidgetPerUsers/ChangeOrder/?stringIds=' + ids.join(","), null, null, 'json');
        };
        $scope.addWidget = function (widget) {
            $scope.widgets.push(widget);
            widget.data = WidgetDataFactory.Create(widget.type);
            widget.update = function() {
                $.getJSON(widget.dataUrl)
                 .done(function (data) {                    
                    widget.data.update(data);

                     /*if (widget.type == 'Bar') {
                        var isNew = !widget.value;
                        if (isNew) {
                            widget.value = ChartFactory.CreateBarChart();
                        }
                        for (var i = 0; i < data.length; i++) {
                            if (!isNew) {
                                widget.value.data.rows[i].c[1].v = data[i];
                            } else {
                                widget.value.data.rows.push({
                                    c: [
                                        { v: i + "" },
                                        { v: data[i] }
                                    ]
                                });
                            }
                        }
                    }
                     if (widget.type == 'Number') {
                         widget.value = data;
                     }*/
                     $scope.$apply(widget);
                 });
            }
            if (widget.updateInterval) {
                setInterval(function() {
                    widget.update();
                }, widget.updateInterval);
            }
        };

        angular.forEach(widgetsPreload, function (value, key) {
            $scope.addWidget(value);
        });

        $scope.sortableOptions = {
            placeholder: "tile block-size-1 half-opac",
            cursor: "move",
            stop: function(event, ui) {
                $scope.saveOrderToApi();
            }
        };
});