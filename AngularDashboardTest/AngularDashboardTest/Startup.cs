﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AngularDashboardTest.Startup))]
namespace AngularDashboardTest
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
